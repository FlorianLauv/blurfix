<?php

require_once './vendor/autoload.php';

$loader = new \Twig\Loader\FilesystemLoader('./views');
$twig = new \Twig\Environment($loader, [
    'cache' => false,
]);

$pagination = [
    'previous' => 'index.php?page=1',
    'next' => 'index.php?page=2',
    'total' => '80',
    'from' => '1',
    'to' => '20',
];

$categories = [
    'all' => 'Tous',
    'anime' => 'Dessins animés',
    'suspens' => 'Suspens',
    'others' => 'Autres',
];

$promote_movie = [
        'title' => 'Toy-story : Dans la chambre d\'Andy, ses jouets se mettent à vivre leur propre vie dès qu\'il sort de la pièce...',
        'picture' => '../assets/img/toystory.jpg',
];

$movies = [
    [
        'title' => 'Les minions contre-attaquent !',
        'picture' => '../assets/img/minion.jpg',
        'favorite' => false,
        'exclusivity' => false,
        'views_number' => 0,
        'category' => 'anime',
    ],
    [
        'title' => "Buzz l'éclair, le cow-boy de l'espace",
        'picture' => '../assets/img/buzz.jpg',
        'favorite' => true,
        'exclusivity' => true,
        'views_number' => 0,
        'category' => 'anime',
    ],
    [
        'title' => "Spider-man : L'homme araignée",
        'picture' => '../assets/img/spiderman.jpg',
        'favorite' => true,
        'exclusivity' => false,
        'views_number' => 0,
        'category' => 'anime',
    ],
    [
        'title' => "Thor : Le marteau ou l'enclume",
        'picture' => '../assets/img/thor.jpg',
        'favorite' => false,
        'exclusivity' => false,
        'views_number' => 0,
        'category' => 'suspens',
    ],
];

$template = $twig->load('home.html.twig');
echo $template->render([
    'movies' => $movies,
    'categories' => $categories,
    'promote_movie' => $promote_movie,
    'pagination' => $pagination,
]);
