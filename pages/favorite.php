<?php

require_once '../vendor/autoload.php';

$loader = new \Twig\Loader\FilesystemLoader('../views');
$twig = new \Twig\Environment($loader, [
    'cache' => false,
]);

$movies = [
    [
        'title' => 'Les minions contre-attaquent !',
        'picture' => '../assets/img/minion.jpg',
        'favorite' => false,
        'exclusivity' => false,
        'views_number' => 0,
    ],
    [
        'title' => "Buzz l'éclair, le cow-boy de l'espace",
        'picture' => '../assets/img/buzz.jpg',
        'favorite' => true,
        'exclusivity' => true,
        'views_number' => 0,
    ],
    [
        'title' => "Spider-man : L'homme araignée",
        'picture' => '../assets/img/spiderman.jpg',
        'favorite' => true,
        'exclusivity' => false,
        'views_number' => 0,
    ],
    [
        'title' => "Thor : Le marteau ou l'enclume",
        'picture' => '../assets/img/thor.jpg',
        'favorite' => false,
        'exclusivity' => false,
        'views_number' => 0,
    ],
];

$template = $twig->load('favorite.html.twig');
echo $template->render([
    'movies' => $movies,
]);
