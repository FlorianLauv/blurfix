<?php

require_once '../vendor/autoload.php';

$loader = new \Twig\Loader\FilesystemLoader('../views');
$twig = new \Twig\Environment($loader, [
    'cache' => false,
]);

$movie =
    [
        'title' => 'Les minions contre-attaquent !',
        'description' => 'Les minions sont de retours, pour vous jouer un mauvais tour...',
        'picture' => '../assets/img/minion.jpg',
        'favorite' => false,
        'exclusivity' => false,
        'views_number' => 0,
        'link' => '../assets/img/minion.mp4',
        'category' => 'anime',
    ];

$template = $twig->load('movie.html.twig');
echo $template->render([
    'movie' => $movie,
]);
