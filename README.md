# Blurfix

## Installation

```shell
composer install
```

## Instructions

Il faut coder la partie back de ce site internet.

### Before

Le chef de projet crée le dépôt GIT dans le groupe `working`.
Il explique la façon dont le travail va se dérouler à son équipe.

Le chef d'équipe crée son tableau KANBAN avec toutes les tâches à réaliser
et la durée de celle-ci. Il établit un planning qu'il devra remettre.

### Model

En premier lieu, réaliser un diagramme UML de votre modélisation de base de données
et le faire valider.

Cela consiste à lister toutes les tables dont vous aurez besoin avec les différents champs
ainsi que de dessiner les relations entre elles.

Votre diagramme doit apparaitre dans le dépôt Git.

### Database

Créer votre base de données et vos tables.

Peupler votre base de données avec des données.

Vous devez le faire en PHP dans un fichier `migrations.php` à la racine du projet.

### Coding

Codez !

Les fonctionnalités à implémenter sont listées [ici](features.md).

### Notes

Votre projet doit contenir un `README.md` avec de la documentation technique de votre projet.
